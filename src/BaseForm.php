<?php
namespace Maksoft\Form;


class BaseForm
{
    public $attributes = array(
        'action' => false,
        'name' => false,
        'enctype' => "application/x-www-form-urlencoded",
        'method' => "POST",
        'id' => false,
    );
    protected $cleaned_data=array();
    protected $fields = array();
    protected $post = array();

    /**
     * Loops over each element in the defined object variables check if there is some private or protected
     * properties and add all of them in fields array.
     * Second Loop check if form_data is not null and iterate over it. If form_data[KEY] is equal to
     * some of object variables check if this var is object. If is true add value to object (eg. for validation)
     *
     *
     * @param array $arr
     * @return none
     */

    public function __construct($post_data=null, $files=null){
        $this->post = $post_data;
        foreach(get_object_vars($this) as $instance=>$value){
            if($value instanceof \Maksoft\Form\Field\Base){
                $value->name = $instance;
                $this->{$instance} = $value;
                $this->fields[$instance] = $this->{$instance};
            }
        }

        $this->clean_files($files);

    }

    public function start() {
        $str = '';
        foreach ($this->attributes as $attribute_name => $attribute_value){
            if(!$attribute_value){ continue; }
            $str .= sprintf("%s=\"%s\" ", $attribute_name, $attribute_value);
        }
        return sprintf("<form %s>", $str);
    }

    public function end() {
        return "</form>";
    }

    protected function clean_files($files){
        if(empty($files)){
            return;
        }

        foreach($files as $file_input_name => $file_arr){
            if(in_array($file_input_name, $this->fields)){
                $this->fields->files = $file_arr;
            }
        }
        return;
    }

    protected function clean_request($post_data)
    {
        /*
         * TODO$
         * array array_diff ( array $array1 , array $array2 [, array $... ] )
         * Compares array1 against one or more other arrays and returns the
         * values in array1 that are not present in any of the other arrays.
         */

        foreach($post_data as $key=>$value){
            if(!array_key_exists($key, $this->fields)){ continue; }
            $this->fields[$key]->value = $value;
        }
    }

    public function is_valid()
    {
        $this->clean_request($this->post);

        foreach($this->cleaned_data as $field_name => $field){
            $field->is_valid();
            $custom_validator = sprintf("validate_%s", $field_name);
            if(method_exists($field, $custom_validator)){
                $field->$custom_validator($field->value);
            }
            $this->cleaned_data[$field_name] = $field->value;
        }

        return true;
    }

    /**
     * Return string representation of form
     *
     * @param null
     * @return string
     */
    public function __toString()
    {
        $tmp = '';
        foreach (get_object_vars($this) as $input_field):
            if(is_object($input_field)):
                $tmp.= (string) $input_field;
            endif;
        endforeach;
        return $tmp;
    }

	public function save()
	{
        if(array_key_exists("csrf", $this->cleaned_data)){
            unset($this->cleaned_data['csrf']);
        }
        return $this->cleaned_data;
	}

    public function setAction($url){
        $this->attributes['action'] = $url;
    }

    public function getAction(){
        if(!$this->attributes['action']){
            return '';
        }
        return $this->attributes['action'];
    }

    public function setMethod($method){
        $this->attributes['method'] = $method;
    }

    public function getmethod(){
        return $this->attributes['method'];
    }

    public function setName($name){
        $this->attributes['name'] = $name;
    }

    public function getName(){
        if(empty($this->attributes['name'])){
            return '';
        }
        return $this->attributes['name'];
    }

    public function setId($id){
        $this->attributes['id'] = $id;
    }

    public function getId()
    {
        return $this->attributes['id'];
    }

    public function clean_data()
    {
        return $this->cleaned_data;
    }
}
?>
